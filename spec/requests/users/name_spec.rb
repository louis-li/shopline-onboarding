require 'rails_helper'

RSpec.describe "GET /users/:id/name", type: :request do
  context "user not exist" do
    it "response 404" do
      get name_user_path(id: "-1")

      expect(response).to have_http_status(404)
    end
  end

  it "get the user name" do
    user = create(:user)

    get name_user_path(user)

    expect(response).to have_http_status(200)
  end
end
