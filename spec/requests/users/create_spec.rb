require 'rails_helper'

RSpec.describe "POST /users", type: :request do
  let(:params) {
    {
      first_name: "Jacky",
      last_name: "Chen",
      address: { 
        country: "china",
        address1: "Guangdong",
        address2: "Shenzhen"
      }
    }
  }

  context "invalid paramter address" do
    it "not acceptable" do
      params[:address] = { foo: "abc" }

      post "/users.json", user: params

      expect(response).to have_http_status(406)
    end
  end
end
