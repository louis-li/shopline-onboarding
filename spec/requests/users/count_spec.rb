require 'rails_helper'

RSpec.describe "GET /users/count", type: :request do
  context "no users" do
    it "response correct count: 0" do
      get count_users_path

      expect(response).to have_http_status(200)
      expect(response.body).to eql("0")
    end
  end

  context "user exisits" do
    it "response correct count" do
      4.times { create(:user) }

      get count_users_path

      expect(response).to have_http_status(200)
      expect(response.body).to eql("4")
    end
  end
end
