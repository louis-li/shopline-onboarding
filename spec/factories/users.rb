FactoryGirl.define do
  factory :user do
    first_name 'Tom'
    last_name 'Cat'
    age 18
  end
end
