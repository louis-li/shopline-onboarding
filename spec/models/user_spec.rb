require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_numericality_of(:age).to_allow(only_integer: true, greater_than: 0) }
  it { should enumerize(:gender) }
  it { is_expected.to embed_one(:shop) }

  describe "#name" do
    it "print first_name and last_name" do
      user = create(:user, first_name: "Stephen", last_name: "Curry")

      expect(user.name).to eql "Stephen Curry"
    end
  end
end
