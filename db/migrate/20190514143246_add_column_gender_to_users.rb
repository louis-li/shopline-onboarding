class AddColumnGenderToUsers < ActiveRecord::Migration
  def change
    add_column :users, :gender, :string, default: 'others'
  end
end
