json.id user.id.to_s
json.first_name user.first_name
json.last_name user.last_name
json.age user.age
json.address user.address
json.created_at user.created_at
json.updated_at user.updated_at
json.url user_url(user, format: :json)
