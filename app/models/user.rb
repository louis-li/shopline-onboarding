class User
  include Mongoid::Document
  include Mongoid::Timestamps
  field :first_name, type: String
  field :last_name, type: String
  field :age, type: Integer
  field :address, type: Hash

  embeds_one :shop

  extend Enumerize

  enumerize :gender, in: %w(male femal others)
  validates_presence_of :first_name, :last_name
  validates :age, numericality: { greater_than: 0, only_integer: true }

  def name
    "#{first_name} #{last_name}"
  end
end
